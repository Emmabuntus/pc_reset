#!/bin/bash

# Détermination de l'utilisateur en fonction du contenu du fichier /etc/lightdm/lightdm.conf
# user_config=emmabuntus
user_config=$(cat /etc/lightdm/lightdm.conf | grep "^autologin-user=" | cut -d = -f2)

if [[ ${user_config} == "" ]] ; then

    user_config=$(who | cut -d ' ' -f1)

fi

if [[ ${user_config} != "" ]] ; then

    dir_config=/home/${user_config}

    rm -R ${dir_config}

    mkdir ${dir_config}

    cp -R /etc/skel/. ${dir_config}

    chown -R ${user_config}:${user_config} ${dir_config}

    chmod -R u+rw ${dir_config}

fi

