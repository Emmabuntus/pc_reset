#!/bin/bash

pwd=$(pwd)

(

    echo "# Start ..."
    pkexec ${pwd}/config_install_PC_clean_demarrage.sh ${pwd}

) |
zenity --progress \
  --title="nstall PC clean demarrage" \
  --text="Start ..." \
  --width=500 \
  --percentage=0


echo "# Installation terminée, vous pouvez éteindre le PC"
zenity --info --text="Installation terminée, vous pouvez éteindre le PC" --width=500
exit 0

