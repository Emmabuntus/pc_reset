#!/bin/bash

dir_soft=$1

if [[ ${dir_soft} != "" ]] ; then
cd ${dir_soft}
fi

echo "$(pwd)"

# Mise en place du lanceur au démarrage dans la config de Lightdm
mod_file=/etc/lightdm/lightdm.conf
if test -f ${mod_file}
then

    sed s/.*session-setup-script=[^$]*/session-setup-script=\\/usr\\/bin\\/emmabuntus_init_user.sh/ ${mod_file} > ${mod_file}.tmp

    cp ${mod_file}.tmp ${mod_file}
    rm ${mod_file}.tmp
    chmod 644 ${mod_file}

fi

# Install script de nettoyage du compte et du repeuplement du compte
cp emmabuntus_init_user.sh /usr/bin/.
chmod 755 /usr/bin/emmabuntus_init_user.sh

# Copie de la config post-installation
cp cairo-dock-choise-dock.conf /etc/skel/.config/cairo-dock-language/
cp -R ~/.config/gtk-3.0 /etc/skel/.config
cp  x*.xml /etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/.
cp emmabuntus/*.* /etc/skel/.config/emmabuntus/.

# Copie par exemple de la config de Firefox
tar -xvf mozmoz -C /etc/skel/



