# Présentation

Le but de ce script est de permettre la réinitialisation au démarrage de l’environnement de bureau d'un PC sous Emmabuntüs dans le cadre d'une utilisation dans une médiathèque.

Ce script a été développé pour répondre au besoin d'installer des [PC en libre-service à la médiathèque d'Opio par Vincent de Linux-Azur](https://blog.linux-azur.org/?p=431).

<p align="center">
![PC en libre-service de la médiathèque d'Opio](/uploads/73810620b25b1c3e5ec3212d1c6c7c3a/opiomed-640x480.jpeg)
</p>


# Utilisation

Copiez le contenu du répertoire scripts sur une clé USB.

Mettre en route un PC sous Emmabuntüs, puis insérez la clé USB, et double cliquez sur le lanceur "Install PC clean demarrage".

Ensuite, vous pouvez éteindre votre PC, et le script de nettoyage effectuera le nettoyage de la session à chaque démarrage.


# Déinstallation

Editez le fichier suivant /etc/lightdm/lightdm.conf :

`sudo geany /etc/lightdm/lightdm.conf`

Mettre en commentaire cette ligne "session-setup-script=/usr/bin/emmabuntus_init_user.sh" en mettant un # devant, puis enregistrez le fichier.

